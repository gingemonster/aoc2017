var test1 = 1024; // 31 steps
var test2 = 12;
var test3 = 23;
var test4 = 31;
var input = 368078;

var currentmemLocation = [0,0];
var currentmemIndex = 1;
var directions = [[1,0],[0,1],[-1,0],[0,-1]];
var currentDirectionIndex = 0;
var movedistance = 1;
var movesmadeinsamedirection = 0;

while(currentmemIndex < input){
  // increment current memory location
  currentmemIndex++;
  
  // get current move 
  var move = directions[currentDirectionIndex % 4];
  // move from current location
  currentmemLocation = [currentmemLocation[0] + move[0], currentmemLocation[1] + move[1]];

  // increment current moves made in this direction
  movesmadeinsamedirection++;

  // check if all moves in this direction completed
  if(movesmadeinsamedirection == movedistance){
    // increment lastmove distance every 2 turns
    if(!((currentDirectionIndex + 1) % 2)){
      movedistance++;
    }    
    currentDirectionIndex++;
    movesmadeinsamedirection = 0;
  }
}

var finallocation = currentmemLocation;
var finalmovesaway = Math.abs(currentmemLocation[0]) + Math.abs(currentmemLocation[1]);
console.log("memory location " + currentmemIndex + " is " + finalmovesaway + " moves away");
