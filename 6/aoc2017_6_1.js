let test = `0,2,7,0`.split(",");
let input = `10	3	15	10	5	15	5	15	9	2	5	8	5	2	3	6`.split("	");

function whenDoesItLoop(input){
  let prevstores = [];
  let currentBankState = input;
  let numRedistributions = 0;
  while(prevstores[currentBankState.toString()] == undefined){
    prevstores[currentBankState.toString()] = 1;
    let largestIndex = getLargestBankIndex(currentBankState)
    currentBankState = distributeValuesFromBank(currentBankState, largestIndex);
    numRedistributions++
  }
  return numRedistributions;
}

function distributeValuesFromBank(banks, startbankindex){
  let result = banks.map((x) => parseInt(x));
  let valueToDistribute = result[startbankindex];
  let currentIndex = startbankindex + 1;
  result[startbankindex] = 0; // zero selected index
  while(valueToDistribute>0){
    if(currentIndex > result.length - 1) {
      currentIndex = 0;
    }
    result[currentIndex] = result[currentIndex] + 1;
    currentIndex++;
    valueToDistribute--;
  }
  return result;
}

function getLargestBankIndex(banks){
  let sortableArray = banks.map((x,index) => [x,index]);
  sortableArray.sort((a,b) => b[0] - a[0] || a[1] - b[1]);
  return sortableArray[0][1];
}

console.log(whenDoesItLoop(input));